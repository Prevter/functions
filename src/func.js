const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string")
    return false;
  if (isNaN(str1) || isNaN(str2))
    return false;
  return `${+str1 + +str2}`;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (let post of listOfPosts) {
    if (post.author === authorName) posts++;
    if (!post.comments) continue;
    for (let comment of post.comments)
      if (comment.author === authorName)
        comments++;
  }
  return `Post:${posts},comments:${comments}`;
};

const tickets = (people) => {
  let money = 0;
  for (let person of people) {
    let change = person - 25;
    if (change > money)
      return "NO";
    money += 25 - change;
  }
  return "YES";
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
